import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { setClientApi } from "./api/apiClient";
import { createRestClientApi } from "./api/apiRest";

export const serverUrl = "http://localhost:8080";
setClientApi(createRestClientApi({ baseUrl: serverUrl }));

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
