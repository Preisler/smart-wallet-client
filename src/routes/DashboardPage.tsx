import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { RouteComponentProps } from "react-router";
import {BudgetItemsStore} from "./mainPage/budgetItems/store";
import {observer} from "mobx-react";

interface IProps extends RouteComponentProps {}

@observer
class Dashboard extends React.Component<IProps> {
    private budgetItemsStore: BudgetItemsStore;

    constructor(props: IProps) {
        super(props);
        this.budgetItemsStore = new BudgetItemsStore();
    }

    componentDidMount(): void {
        this.budgetItemsStore.init();
    }

    render() {
        return <div></div>
    }
}

export default Dashboard;
