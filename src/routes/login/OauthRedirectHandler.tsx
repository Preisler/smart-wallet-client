import React, { Component } from "react";

import { Redirect, RouteComponentProps } from "react-router-dom";

interface IProps extends RouteComponentProps {}

class OAuth2RedirectHandler extends Component<IProps> {
    getUrlParameter(name: string) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");

        var results = regex.exec(this.props.location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    render() {
        const token = this.getUrlParameter("token");
        const error = this.getUrlParameter("error");

        if (token) {
            localStorage.setItem("accessToken", token);
            return (
                <Redirect
                    to={{
                        pathname: "/",
                        state: { from: this.props.location }
                    }}
                />
            );
        } else {
            return (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: {
                            from: this.props.location,
                            error: error
                        }
                    }}
                />
            );
        }
    }
}

export default OAuth2RedirectHandler;
