import React, { Component } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { Theme, withStyles, WithStyles, createStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import GoogleButton from "react-google-button";
import { serverUrl } from "../..";
import { Copyright } from "../../components/components";

const styles = (theme: Theme) =>
    createStyles({
        "@global": {
            body: {
                backgroundColor: theme.palette.common.white
            }
        },
        paper: {
            marginTop: theme.spacing(8),
            display: "flex",
            flexDirection: "column",
            alignItems: "center"
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main
        }
    });

interface IProps extends WithStyles<typeof styles> {}

class Login extends Component<IProps> {
    render() {
        const { classes } = this.props;

        return (
            <Container>
                <Grid container xs={12} justify={"center"} alignContent={"center"}>
                    <Grid item xs={10} lg={6}>
                        <Paper>
                            <CssBaseline />
                            <div className={classes.paper}>
                                <Typography component="h1" variant="h5">
                                    Sign In
                                </Typography>

                                <GoogleButton onClick={() => (window.location.href = `${serverUrl}/oauth2/authorize/google`)} />
                            </div>
                            <Box mt={8}>
                                <Copyright />
                            </Box>
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        );
    }
}

export default withStyles(styles)(Login);
