import React, {Component, ErrorInfo} from "react";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {Link, Redirect} from "react-router-dom";
import { BudgetsStore } from "./store";
import { observer } from "mobx-react";
import Button from "@material-ui/core/Button";
import { Add, Delete, Close } from "@material-ui/icons/";
import { Alert } from "@material-ui/lab";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Toolbar from "@material-ui/core/Toolbar";
import * as colors from "@material-ui/core/colors";
import {ButtonGroup, IconButton, Collapse} from "@material-ui/core";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import CustomAlert from "../../../components/customAlert";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";

interface IProps {}

interface IState {
    openCreateModal: boolean;
    error: string;
    openErr: boolean;
    openAlertSuccess: boolean;
    isLoading: boolean;
}

@observer
class BudgetsPage extends Component<IProps, IState> {
    private _store: BudgetsStore;
    constructor(props: IProps) {
        super(props);
        this._store = new BudgetsStore();
        this.state = { openCreateModal: false, error: "", openErr: true, openAlertSuccess: false, isLoading: true };
        this.handleModalOpen = this.handleModalOpen.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAlertSuccess = this.handleAlertSuccess.bind(this);
    }
    async componentDidMount() {
        await this._store.init();
        this.setState({
            isLoading: false
        })
    }

    handleModalOpen(): void {
        this.setState({ openCreateModal: true });
    }

    handleModalClose(): void {
        this.setState({ openCreateModal: false });
        this._store.resetBudget();
    }

    handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        this._store.newBudget.name = event.target.value;
    }

    handleAlertSuccess(): void {
        this.setState({openAlertSuccess: !this.state.openAlertSuccess})
    }

    async handleSave(): Promise<void> {
        try {
            await this._store.saveBudget();
            this.setState({openAlertSuccess: true})
        } catch (e) {
            console.log(e)
        }
        this.handleModalClose();
        this._store.resetBudget();
    }

    async handleDelete(): Promise<void> {
        try {
            await this._store.deleteBudget();
        } catch (e) {
            console.log(e);
            this.setState({
                error: e.data.message
            });
        }
    }

    handleClick(index: number): void {
        this._store.selectedItemIndex = index;
    }

    redirectToItems = () => {
        debugger;
        return (
            <Redirect to={{
                pathname: "/dashboard",
                state: {budgetId: 8}
            }}/>
        )
    }

    render() {
        return [
            <Box display="flex" justifyContent="center">
                {this.state.isLoading && (
                    <CircularProgress/>
                )}
            </Box>,
            <Toolbar>
                <ButtonGroup variant={"text"}>
                    <Button startIcon={<Add />} onClick={this.handleModalOpen}>
                        Add
                    </Button>
                    {this._store.selectedItemIndex !== undefined && (
                        <Button startIcon={<Delete />} onClick={this.handleDelete}>
                            Delete
                        </Button>
                    )}
                </ButtonGroup>
            </Toolbar>,

            <Paper>
                <CustomAlert text="Created" severity="success" openErr={this.state.openAlertSuccess} handleOpen={this.handleAlertSuccess}/>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell id="name">Name</TableCell>
                            <TableCell id="owner">Owner Name</TableCell>
                            <TableCell id="actions">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this._store.budgets.map((b, i) => {
                            return (
                                <TableRow
                                    key={b.id}
                                    role="checkbox"
                                    hover
                                    selected={this._store.selectedItemIndex === i}
                                    onClick={() => this.handleClick(i)}
                                >
                                    <TableCell>{b.name}</TableCell>
                                    <TableCell>{b.owner.name}</TableCell>
                                    <TableCell><Link to={{
                                        pathname:"/item",
                                        state:{budgetId: b.id
                                        }
                                    }}><IconButton><ArrowForwardIosIcon fontSize={"small"}/></IconButton></Link></TableCell>

                                </TableRow>

                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>,
            <Dialog open={this.state.openCreateModal} onClose={this.handleModalClose} fullWidth>
                <DialogTitle>Create New Budget</DialogTitle>
                <DialogContent>
                    <TextField
                        id="outlined-basic"
                        label="Budget Name"
                        variant="outlined"
                        fullWidth
                        value={this._store.newBudget.name}
                        onChange={this.handleChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleSave} color="primary">
                        Create
                    </Button>
                    <Button onClick={this.handleModalClose} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        ];
    }
}

export default BudgetsPage;
