import * as api from "../../../api/api";
import { observable } from "mobx";
import { IBudgetDto } from "../../../api/api";

export class BudgetsStore {
    @observable private _budgets: IBudgetDto[] = [];
    @observable newBudget: IBudgetDto;
    @observable selectedItemIndex?: number;

    constructor() {
        this.newBudget = this.createDefaultBudget();
    }

    async init() {
        this.createDefaultBudget();
        await this.loadBudgets();
    }

    get budgets() {
        return this._budgets;
    }

    async loadBudgets(): Promise<void> {
        try {
            const response = await api.getClientApi().sendRequest(api.GetBudgets({}));
            this.setBudgets(response);
        } catch (e) {
            throw e;
        }
    }

    async saveBudget(): Promise<void> {
        try {
            await api.getClientApi().sendRequest(api.CreateBudget(this.newBudget));
        } catch (e) {
            throw e;
        } finally {
            this.loadBudgets();
        }
    }

    async deleteBudget(): Promise<void> {
        if (this.selectedItemIndex !== undefined) {
            const item = this._budgets[this.selectedItemIndex];
            try {
                await api.getClientApi().sendRequest(api.DeleteBudget({ id: item.id }));
            } catch (e) {
                throw e;
            } finally {
                this.selectedItemIndex = undefined;
                this.loadBudgets();
            }
        }
    }

    resetBudget() {
        this.newBudget = this.createDefaultBudget();
    }

    private setBudgets(budgets: IBudgetDto[]): void {
        this._budgets = budgets;
    }

    private createDefaultBudget(): IBudgetDto {
        return {
            id: 0,
            name: "",
            owner: { Id: 0, email: "test@test.cz", name: "John Doe", userType: "", imageUrl: "", budgets: [] }
        };
    }
}
