import React, {CSSProperties} from "react";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Theme, withStyles, WithStyles, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Dashboard from "../DashboardPage";
import { Route, RouteComponentProps } from "react-router-dom";
import { List, ListItem, ListItemText, ListItemIcon } from "@material-ui/core";
import BudgetsPage from "./budgets/budgetsPage";
import BudgetItemsPage from "./budgetItems/budgetItemsPage";

const styles = (theme: Theme) =>
    createStyles({
        root: {
            display: "flex"
        },
        toolbar: {
            paddingRight: 24
        },
        toolbarIcon: {
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            padding: "0 8px",
            ...theme.mixins.toolbar
        },
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
            transition: theme.transitions.create(["width", "margin"], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen
            })
        },
        appBarShift: {
            marginLeft: drawerWidth,
            width: `calc(100% - ${drawerWidth}px)`,
            transition: theme.transitions.create(["width", "margin"], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            })
        },
        menuButton: {
            marginRight: 36
        },
        menuButtonHidden: {
            display: "none"
        },
        title: {
            flexGrow: 1
        },
        drawerPaper: {
            position: "relative",
            whiteSpace: "nowrap",
            width: drawerWidth,
            transition: theme.transitions.create("width", {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            })
        },
        drawerPaperClose: {
            overflowX: "hidden",
            transition: theme.transitions.create("width", {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen
            }),
            width: theme.spacing(7),
            [theme.breakpoints.up("sm")]: {
                width: theme.spacing(9)
            }
        },
        appBarSpacer: theme.mixins.toolbar as any,
        content: {
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
            padding: 25
        },
        container: {
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(4)
        },
        paper: {
            padding: theme.spacing(2),
            display: "flex",
            overflow: "auto",
            flexDirection: "column"
        },
        fixedHeight: {
            height: 240
        }
    });

interface IProps extends RouteComponentProps, WithStyles<typeof styles> {}

interface IState {
    isLeftMenuOpen: boolean;
}

const drawerWidth = 240;

class MainPage extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = { isLeftMenuOpen: true };

        this.handleLogout = this.handleLogout.bind(this);
    }

    handleDrawerOpen() {
        this.setState({ isLeftMenuOpen: true });
    }
    handleDrawerClose() {
        this.setState({ isLeftMenuOpen: false });
    }

    handleLogout() {
        localStorage.removeItem("accessToken");
        this.props.history.push("/login");
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <CssBaseline />
                <AppBar position="absolute" className={clsx(classes.appBar, this.state.isLeftMenuOpen && classes.appBarShift)}>
                    <Toolbar className={classes.toolbar}>
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="open drawer"
                            onClick={() => this.handleDrawerOpen()}
                            className={clsx(classes.menuButton, this.state.isLeftMenuOpen && classes.menuButtonHidden)}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Wallet
                        </Typography>
                        <IconButton color="inherit" onClick={this.handleLogout}>
                            <ExitToAppIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: clsx(classes.drawerPaper, !this.state.isLeftMenuOpen && classes.drawerPaperClose)
                    }}
                    open={this.state.isLeftMenuOpen}
                >
                    <div className={classes.toolbarIcon}>
                        <IconButton onClick={() => this.handleDrawerClose()}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>{this.getSidebarItems()}</List>
                    <Divider />
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.appBarSpacer} />
                    <div>
                        <Route path={"/dashboard"} component={Dashboard} />
                        <Route path={"/budgets"} component={BudgetsPage} />
                        <Route path={"/item"} component={BudgetItemsPage}/>
                    </div>
                </main>
            </div>
        );
    }

    private getSidebarItems(): React.ReactNode {
        return [
            <ListItem button key={"dashboard"}>
                <ListItemIcon>
                    <DashboardIcon />
                </ListItemIcon>
                <ListItemText primary="Dashboard" onClick={() => this.props.history.push("/dashboard")} />
            </ListItem>,
            <ListItem button key={"budgets"}>
                <ListItemIcon>
                    <MonetizationOnIcon />
                </ListItemIcon>
                <ListItemText primary="Budgets" onClick={() => this.props.history.push("/budgets")} />
            </ListItem>
        ];
    }
}

export default withStyles(styles)(MainPage);
