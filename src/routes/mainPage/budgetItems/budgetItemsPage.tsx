import {observer} from "mobx-react";
import React, {Component} from "react";
import Toolbar from "@material-ui/core/Toolbar";
import {ButtonGroup, MenuItem} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Add, Delete} from "@material-ui/icons";
import Paper from "@material-ui/core/Paper";
import CustomAlert from "../../../components/customAlert";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import {BudgetItemsStore} from "./store";
import {CategoryStore} from "../categories/store";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import {ICategoryDto} from "../../../api/apiDtos";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";

interface IProps {
    location: any;
}

interface IState {
    openCreateModal: boolean;
    error: string;
    openErr: boolean;
    openAlertSuccess: boolean;
    isLoading: boolean;
}

@observer
class BudgetItemsPage extends Component<IProps, IState> {
    private _itemStore: BudgetItemsStore;
    private _categoryStore: CategoryStore;
    constructor(props: IProps) {
        super(props);
        this._itemStore = new BudgetItemsStore();
        this._categoryStore = new CategoryStore();
        this.state = { openCreateModal: false, error: "", openErr: true, openAlertSuccess: false, isLoading: true };
        this.handleModalOpen = this.handleModalOpen.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
        this.handleChangeOfName = this.handleChangeOfName.bind(this);
        this.handleChangeOfPrice = this.handleChangeOfPrice.bind(this);
        this.handleChangeOfCategory = this.handleChangeOfCategory.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAlertSuccess = this.handleAlertSuccess.bind(this);
    }
    async componentDidMount() {
        console.log(this.props);
        this._itemStore.budgetId = this.props.location.state.budgetId;
        debugger;
        await this._itemStore.init();
        this.setState({
            isLoading: false
        });
        this._categoryStore.init();
    }

    handleModalOpen(): void {
        this.setState({ openCreateModal: true });
    }

    handleModalClose(): void {
        this.setState({ openCreateModal: false });
        this._itemStore.resetBudget();
    }

    handleChangeOfName(event: React.ChangeEvent<HTMLInputElement>): void {
        this._itemStore.newBudgetItem.itemName = event.target.value;
    }

    handleChangeOfPrice(event: React.ChangeEvent<HTMLInputElement>): void {
        let price: number = parseFloat(event.target.value);
        this._itemStore.newBudgetItem.price = price;
    }

    handleChangeOfCategory(event: any): void {
        this._itemStore.newBudgetItem.categoryId = event.target.value;
    }

    handleAlertSuccess(): void {
        this.setState({openAlertSuccess: !this.state.openAlertSuccess})
    }

    async handleSave(): Promise<void> {
            try {
                await this._itemStore.saveBudget();
                this.setState({openAlertSuccess: true})
            } catch (e) {
                console.log(e)
            }
            this.handleModalClose();
            this._itemStore.resetBudget();
    }

    async handleDelete(): Promise<void> {
        try {
            await this._itemStore.deleteBudget();
        } catch (e) {
            console.log(e);
            this.setState({
                error: e.data.message
            });
        }
    }

    handleClick(index: number): void {
        this._itemStore.selectedItemIndex = index;
    }

    render() {
        return [
            <Box display="flex" justifyContent="center">
                {this.state.isLoading && (
                    <CircularProgress/>
                )}
            </Box>,
            <Toolbar>
                <ButtonGroup variant={"text"}>
                    <Button startIcon={<Add />} onClick={this.handleModalOpen}>
                        Add
                    </Button>
                    {this._itemStore.selectedItemIndex !== undefined && (
                        <Button startIcon={<Delete />} onClick={this.handleDelete}>
                            Delete
                        </Button>
                    )}
                </ButtonGroup>
            </Toolbar>,

            <Paper>
                <CustomAlert text="Created" severity="success" openErr={this.state.openAlertSuccess} handleOpen={this.handleAlertSuccess}/>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell id="name">Name</TableCell>
                            <TableCell id="price">Price</TableCell>
                            <TableCell id="category">Category</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this._itemStore.items.map((b, i) => {
                            return (
                                <TableRow
                                    key={b.id}
                                    role="checkbox"
                                    hover
                                    selected={this._itemStore.selectedItemIndex === i}
                                    onClick={() => this.handleClick(i)}
                                >
                                    <TableCell>{b.name}</TableCell>
                                    <TableCell>{b.price}</TableCell>
                                    <TableCell>{b.category.name}</TableCell>
                                </TableRow>
                            );
                        })}
                        <TableRow>
                            <TableCell>Total</TableCell>
                            <TableCell>{this._itemStore.totalPrice}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Paper>,
            <Dialog open={this.state.openCreateModal} onClose={this.handleModalClose} fullWidth>
                <DialogTitle>Create New Item</DialogTitle>
                <DialogContent>
                    <TextField
                        id="outlined-basic"
                        label="Name"
                        variant="outlined"
                        fullWidth
                        value={this._itemStore.newBudgetItem.itemName}
                        onChange={this.handleChangeOfName}
                    />
                    <TextField
                        id="outlined-basic"
                        label="Price"
                        variant="outlined"
                        type="number"
                        fullWidth
                        value={this._itemStore.newBudgetItem.price}
                        onChange={this.handleChangeOfPrice}
                    />
                    <InputLabel id="demo-simple-select-label">Category</InputLabel>
                    {this._categoryStore._items !== undefined &&
                    (
                    <Select
                        value={this._categoryStore.selectedCategory}
                        onChange={this.handleChangeOfCategory}
                        >
                    {this._categoryStore._items.map((item) => {
                        return (
                        <MenuItem value={item.id}>{item.name}</MenuItem>
                        )
                    })}
                        </Select>
                    )}

                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleSave} color="primary">
                        Create
                    </Button>
                    <Button onClick={this.handleModalClose} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        ];
    }
}
export default BudgetItemsPage