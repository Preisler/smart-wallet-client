import * as api from "../../../api/api";
import {computed, observable} from "mobx";
import {IBudgetItemDto, ICategoryDto, IGetBudgetItemDto} from "../../../api/api";

export class BudgetItemsStore {
    @observable private _items: IGetBudgetItemDto[] = [];
    @observable newBudgetItem: IBudgetItemDto;
    @observable selectedItemIndex?: number;
    @observable budgetId?: number;

    constructor() {
        this.newBudgetItem = this.createDefaultBudget();
    }

    @computed get totalPrice():number {
        let total: number = 0;
        for (let i=0; i< this._items.length; i++) {
            total += this._items[i].price;
        }
        return total;
    }

    async init() {
        debugger;
        this.createDefaultBudget();
        await this.loadItems();
    }

    get items() {
        return this._items;
    }

    async loadItems(): Promise<void> {
        try {
            const response = await api.getClientApi().sendRequest(api.getBudgetItems({id: this.budgetId}));
            this.setBudgets(response);
        } catch (e) {
            throw e;
        }
    }

    async saveBudget(): Promise<void> {
        if (this.budgetId) {
            this.newBudgetItem.budgetId = this.budgetId;
        }
        try {
            await api.getClientApi().sendRequest(api.createBudgetItem(this.newBudgetItem));
        } catch (e) {
            throw e;
        } finally {
            this.loadItems();
        }
    }

    async deleteBudget(): Promise<void> {
        if (this.selectedItemIndex !== undefined) {
            const item = this._items[this.selectedItemIndex];
            try {
                await api.getClientApi().sendRequest(api.deleteBudgetItem({ id: item.id }));
            } catch (e) {
                throw e;
            } finally {
                this.selectedItemIndex = undefined;
                this.loadItems();
            }
        }
    }

    resetBudget() {
        this.newBudgetItem = this.createDefaultBudget();
    }

    private setBudgets(budgets: IGetBudgetItemDto[]): void {
        this._items = budgets;
    }

    private createDefaultBudget(): IBudgetItemDto {
        return {
            id: 0,
            itemName: "",
            price: 0,
            categoryId: 0,
            budgetId: 0
        }
    }
}
