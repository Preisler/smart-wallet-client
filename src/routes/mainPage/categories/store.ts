import * as api from "../../../api/api";
import { observable } from "mobx";
import {IBudgetDto, IBudgetItemDto, ICategoryDto} from "../../../api/api";

export class CategoryStore {
    @observable _items: ICategoryDto[] = [];
    @observable selectedCategory?: number;

    constructor() {}

    init() {
        this.loadItems();
    }

    async loadItems(): Promise<void> {
        try {
            debugger;
            const response = await api.getClientApi().sendRequest(api.getCategory({}));
            this.setCategories(response);
        } catch (e) {
            throw e;
        }
    }

    private setCategories(categories: ICategoryDto[]): void {
        this._items = categories;
    }
}
export default CategoryStore;