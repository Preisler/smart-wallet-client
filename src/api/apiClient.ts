export enum HttpMethod {
    Get,
    Post,
    Put,
    Delete
}

export interface IRequestSettings<TData> {
    method: HttpMethod;
    controller: string;
    data?: TData;
}

export interface IApiClient {
    sendRequest: <TRequestData>(request: IRequestSettings<TRequestData>) => Promise<any>;
}

let clientApi: IApiClient;
export function setClientApi(api: IApiClient) {
    clientApi = api;
}
export function getClientApi(): IApiClient {
    return clientApi;
}
