export interface IUserDto {
    Id: number;
    name: string;
    email: string;
    userType: string;
    imageUrl: string;
    budgets: IBudgetDto[];
}

export interface IBudgetDto {
    id: number;
    name: string;
    owner: IUserDto;
}

export interface IGetBudgetItemDto {
    id: number;
    name: string;
    price: number;
    date: string;
    budget: IBudgetDto;
    category: ICategoryDto;
}


export interface IBudgetItemDto {
    id: number;
    itemName: string;
    price: number;
    budgetId: number;
    categoryId: number;
}

export interface ICategoryDto {
    id: number;
    name: string;
}
