import {HttpMethod, IRequestSettings} from "./apiClient";
import {IBudgetDto, IBudgetItemDto} from "./apiDtos";

export interface IGetUsersData {}
export function GetUsers(data?: IGetUsersData): IRequestSettings<IGetUsersData> {
    return {
        controller: "users",
        method: HttpMethod.Get,
        data
    };
}

// Budgets
export function CreateBudget(data: IBudgetDto): IRequestSettings<IBudgetDto> {
    return {
        controller: "budget",
        method: HttpMethod.Post,
        data
    };
}

export interface IGetBudgetsData {
    id?: number;
}
export function GetBudgets(data: IGetBudgetsData): IRequestSettings<IGetBudgetsData> {
    return { controller: "budget", method: HttpMethod.Get, data };
}

export interface IDeleteBudgetData {
    id: number;
}
export function DeleteBudget(data: IDeleteBudgetData): IRequestSettings<IGetBudgetsData> {
    return { controller: "budget", method: HttpMethod.Delete, data };
}


// BudgetItems

export function getBudgetItems(data: IGetBudgetItemData): IRequestSettings<IGetBudgetsData> {
    return { controller: "item", method: HttpMethod.Get, data };
}

export function createBudgetItem(data: IBudgetItemDto): IRequestSettings<IBudgetItemDto> {
    return {
        controller: "item",
        method: HttpMethod.Post,
        data
    };
}

export interface IGetBudgetItemData {
    id?: number
}

export interface IDeleteBudgetItemData {
    id: number;
}

export function deleteBudgetItem(data: IDeleteBudgetItemData): IRequestSettings<IGetBudgetItemData> {
    return {
        controller: "item",
        method: HttpMethod.Delete,
        data
    }
}

// Categories

export interface IGetCategoryData {
    id?: number;
}

export function getCategory(data: IGetCategoryData): IRequestSettings<IGetCategoryData> {
    return { controller: "category", method: HttpMethod.Get, data };
}