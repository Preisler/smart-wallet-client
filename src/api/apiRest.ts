import axios, { AxiosResponse } from "axios";
import { IApiClient, IRequestSettings, HttpMethod } from "./apiClient";

export interface IRestClientSetting {
    baseUrl: string;
}

export function createRestClientApi(settings: IRestClientSetting): IApiClient {
    const baseUrl = settings.baseUrl;
    return {
        sendRequest: <TRequestData>(request: IRequestSettings<TRequestData>) => {
            return new Promise<any>((resolve, reject) => {
                const response = sendBuildedRequest<TRequestData>(baseUrl, request);
                response
                    .then(result => {
                        if (result.status === 200) resolve(result.data);
                        reject(result.status);
                    })
                    .catch(r => {
                        reject(r.response);
                    });
            });
        }
    };
}

function sendBuildedRequest<TRequestData>(baseUrl: string, request: IRequestSettings<TRequestData>): Promise<AxiosResponse<any>> {
    const accessToken = localStorage.getItem("accessToken");

    const config = {
        headers: { Authorization: `Bearer ${accessToken}` }
    };

    switch (request.method) {
        case HttpMethod.Get:
            return axios.get(`${baseUrl}/${request.controller}`, { ...config, params: request.data });
        case HttpMethod.Post:
            return axios.post(`${baseUrl}/${request.controller}`, request.data, config);
        case HttpMethod.Delete:
            return axios.delete(`${baseUrl}/${request.controller}`, { ...config, params: request.data });
        default:
            return axios.get(`${baseUrl}/${request.controller}`, { ...config, params: request.data });
    }
}
