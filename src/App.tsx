import React, { Component } from "react";
import MainPage from "./routes/mainPage/page";
import Login from "./routes/login/Login";
import OAuth2RedirectHandler from "./routes/login/OauthRedirectHandler";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { PrivateRoute } from "./components/components";

export default class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/authorizeRedirect" component={OAuth2RedirectHandler} />
                    <PrivateRoute>
                        <Route path="/" component={MainPage} />
                    </PrivateRoute>
                </Switch>
            </BrowserRouter>
        );
    }
}
