import React, { Fragment } from "react";
import Typography from "@material-ui/core/Typography";
import { Redirect } from "react-router-dom";
import {Alert} from "@material-ui/lab";
import {Collapse} from "@material-ui/core";

export function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {"Copyright © "}
            Semestral project (FIM UHK) {new Date().getFullYear()}
            {"."}
        </Typography>
    );
}

export function PrivateRoute(props: any) {
    return <Fragment>{isUserAuthenticated() ? props.children : <Redirect to={{ pathname: "/login" }} />}</Fragment>;
}

function isUserAuthenticated() {
    const accessToken = localStorage.getItem("accessToken");
    return accessToken !== null;
}
