import React, {Component} from "react";
import {observer} from "mobx-react";
import {Alert} from "@material-ui/lab";
import {Collapse} from "@material-ui/core";

interface IProps {
    text: string;
    openErr: boolean;
    handleOpen: any;
    severity: any;
}

interface IState {
}


class CustomAlert extends Component<IProps, IState>{
    constructor(props: IProps) {
        super(props);
    }

    render() {
        return (
            <Collapse
                in={this.props.openErr}
            >
                <Alert
                    severity={this.props.severity}
                    onClose={this.props.handleOpen}
                >
                    {this.props.text}
                </Alert>
            </Collapse>
        );
    }
}

export default CustomAlert;